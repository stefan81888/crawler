﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using HtmlAgilityPack;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;

namespace Crawler
{
	public static class Utility
	{
		private static XmlDocument _document;

		public static double BytesToMb(double bytes)
		{
			return bytes / (1024 * 1024);
		}

		private static List<string> SplitIntoSentences(string text)
		{
			string[] split = text.Split(new string[] { ",", ".", "!", "?", "  "}, StringSplitOptions.None);
			List<string> splitList = split.ToList();
			splitList.RemoveAll(x => x.Length == 0);
			return splitList;
		}

		public static async Task<List<string>> ExtractDepartments(string text)
		{
			if (text.Length == 0)
				return new List<string>();

			XmlDocument document = new XmlDocument();
			document.Load("../../../crawler.config.xml");

			var conditions = document.GetElementsByTagName("department-condition");

			string parentsFilter = "node => ";

			foreach (XmlNode node in conditions)
			{
				var condition = "\"" + node.InnerText + "\"";
				parentsFilter += "node.Contains(" + condition + ")" + " || ";
			}

			var filter = parentsFilter.Remove(parentsFilter.Length - 3, 3);
			var options = ScriptOptions.Default.AddReferences(typeof(string).Assembly);
			Func<string, bool> criteria = await CSharpScript.EvaluateAsync<Func<string, bool>>(filter, options);


			List<string> split = SplitIntoSentences(text);
			var index = split.FindIndex(x => x.Contains("Departments") || x.Contains("ДЕПАРТМАНИ"));

			if (index >= 0)
			{
				split.RemoveRange(0, index);
				return split;
			}

			var res = split.Where(criteria).ToList();

			return split.Count == res.Count ? split : res;
		}

		public static XmlDocument GetConfig()
		{
			_document = new XmlDocument();
			_document.Load("../../../crawler.config.xml");
			return _document;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Crawler
{
	public class CrawlParameters
	{
		public int Depth { get; set; }
		public HtmlDocument Page { get; set; }
		public List<HtmlNode> Nodes { get; set; }
		public Func<HtmlNode, bool> CrawlCriteria { get; set; }
		public Func<HtmlDocument, List<HtmlNode>> FindNodes { get; set; }
		public string Link { get; set; }

		public CrawlParameters()
		{
			Nodes = new List<HtmlNode>();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Crawler
{
	public class Page
	{
		public List<Page> ChildPages { get; set; }
		public string Link { get; set; }
	}
}

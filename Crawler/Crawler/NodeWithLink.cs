﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Crawler
{
	public class NodeWithLink
	{
		public HtmlNode Node { get; set; }
		public string Link { get; set; }		
	}
}

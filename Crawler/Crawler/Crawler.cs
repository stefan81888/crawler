﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using HtmlAgilityPack;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;


namespace Crawler
{
	public class Crawler
	{
		private ReportWriter _reportWriter;
		public int LinksTraversed { get; set; }
		public double MaxMemoryAllocated { get; set; }
		public int TotalNodesTraversed { get; set; }
		public int NodesRemoved { get; set; }
		public int FacultiesFound { get; set; }
		private int _depth;
		private XmlDocument _document;

		public Crawler()
		{
			this._reportWriter = ReportWriter.Instance;
			_document = Utility.GetConfig();
			string depth = _document.DocumentElement["depth"].InnerText;
			_depth = int.Parse(depth);
		}

		public void WriteReport(string report)
		{
			_reportWriter.WriteReport(report);
		}

		public string GetRootWebsite()
		{
			string root = _document.DocumentElement["seed"].InnerText;
			return root;
		}

		public void InitReportParams()
		{
			LinksTraversed = 0;
			MaxMemoryAllocated = 0;
			TotalNodesTraversed = 0;
			NodesRemoved = 0;
		}

		public HtmlDocument GetPage(string pageUrl) 
		{
			if(!Uri.IsWellFormedUriString(pageUrl, UriKind.RelativeOrAbsolute))
				return null;

			string html;
			HtmlDocument page = new HtmlDocument();

			using (WebClient client = new WebClient())
			{
				client.Encoding = Encoding.UTF8;

				try
				{
					html = client.DownloadString(pageUrl);
					page = new HtmlAgilityPack.HtmlDocument();
				}
				catch (Exception e)
				{
					return page;
				}

				page.LoadHtml(html);
			}

			return page;
		}

		public List<HtmlNode> GetLinks(HtmlDocument document)
		{
			List<HtmlNode> links = document.DocumentNode.Descendants("a").ToList();
			return links;
		}

		public HtmlNode GetTitle(HtmlDocument document)
		{
			List<HtmlNode> links = document.DocumentNode.Descendants("title").ToList();
			return links.FirstOrDefault();
		}

		public List<HtmlNode> GetAllDocumentNodes(HtmlDocument document)
		{
			List<HtmlNode> nodes = document.DocumentNode.Descendants().ToList();
			return nodes;
		}

		public void CrawlPage(ref CrawlParameters parameters)
		{
			LinksTraversed++;

			var allNodes = parameters.FindNodes.Invoke(parameters.Page);
			var totalNodesFound = allNodes.Count;

			TotalNodesTraversed += totalNodesFound;

			var allLinks = allNodes.FindAll(x => x.Name == "a");
			var totalLinks = allLinks.Count;

			string report;

			var relevantNodes = allNodes.Where(parameters.CrawlCriteria).ToList();
			var relevantNodesFound = relevantNodes.Count;
			NodesRemoved += (totalNodesFound - relevantNodesFound);

			var links = relevantNodes.FindAll(x => x.Name == "a");

			var withoutDuplicates = relevantNodes.GroupBy(x => x.InnerText)
				.Select(g => g.OrderBy(o => o.InnerText).First())
				.ToList();


			var linksDedup = links.GroupBy(x => x.Attributes["href"]?.Value)
				.Select(g => g.OrderBy(o => o.Attributes["href"]?.Value).First())
				.ToList();
			var linksCount = linksDedup.Count;

			var memory = Utility.BytesToMb(GC.GetTotalMemory(false));

			if (memory > MaxMemoryAllocated)
				MaxMemoryAllocated = memory;

			report =
				"Total nodes on this page: " + totalNodesFound + " with " + totalLinks + " links" + "\r\n" + 
				"Removed: " + (totalNodesFound - relevantNodesFound) + " nodes, remained " + relevantNodesFound + " relevant nodes" + "\r\n" +
				"Nodes remaining after removing duplicates from relevant nodes: " + withoutDuplicates.Count + "\r\n" +
				"Links remaining after removing duplicates from links in relevant nodes, based on href value: " + linksCount + "\r\n" + 
				"Memory allocated: " + memory + " MB" + "\r\n" +
				"Depth from the root page: " + parameters.Depth + "\r\n";

			this.WriteReport(report);

			parameters.Nodes.AddRange(withoutDuplicates);

			if (parameters.Depth == 0)
				return;

			parameters.Depth--;

			foreach (HtmlNode link in linksDedup)
			{
				string href = link.Attributes["href"]?.Value;

				if(href == null)
					continue;

				var pageLink = CreateLink(parameters.Link ,href);

				report = "Address: " + pageLink;
				WriteReport(report);

				HtmlDocument linkPage = GetPage(pageLink);

				if(linkPage == null)
					continue;

				parameters.Page = linkPage;

				CrawlPage(ref parameters);
			}
		}

		public string CreateLink(string root, string url)
		{
			if (url == null)
				return "";

			if (url.Contains("index.php/en"))
				url = url.Remove(url.IndexOf("index.php/en"), 12);

			if (url.StartsWith("http://") || url.StartsWith("https://"))
				return url;

			if (url.Contains("/en"))
				url = url.Remove(0, 3);

			return root + url;
		}

		public async Task<List<string>> GetFacultiesWebsites(string page)
		{
			CrawlParameters parameters = new CrawlParameters()
			{
				CrawlCriteria = x => x.Name == "a",
				Depth = _depth,
				Page = GetPage(page),
				FindNodes = GetLinks,
				Link = page
			};

			CrawlPage(parameters: ref parameters);

			string startFilter = "node => ";
			var conditions = _document.GetElementsByTagName("faculty");

			foreach (XmlNode node in conditions)
			{
				var condition = "\"" + node.InnerText + "\"";
				startFilter += "node.InnerText.Contains(" + condition + ")" + " || ";
			}

			var filter = startFilter.Remove(startFilter.Length - 3, 3);
			var options = ScriptOptions.Default.AddReferences(typeof(HtmlNode).Assembly);
			Func<HtmlNode, bool> criteria = await CSharpScript.EvaluateAsync<Func<HtmlNode, bool>>(filter, options);

			var faculty = parameters.Nodes.Where(criteria).ToList();
			var facultiesDedup = faculty.GroupBy(x => x.Attributes["href"].Value)
				.Select(g => g.OrderBy(o => o.Attributes["href"].Value).First())
				.ToList();

			NodesRemoved += parameters.Nodes.Count - faculty.Count;

			var res = new List<string>();

			foreach (var facultyDedup in facultiesDedup)
			{
				res.Add(facultyDedup.Attributes["href"].Value);
			}

			FacultiesFound = res.Count;
			return res;
		}

		public void GetChildNodes(List<HtmlNode> nodes, ref List<HtmlNode> result)
		{
			foreach (var node in nodes)
			{
				if(node.ChildNodes.Count == 0)
					continue;

				result.AddRange(node.ChildNodes);
				GetChildNodes(node.ChildNodes.ToList(), ref result);
			}
		}

		#region Department extraction methods

		public List<HtmlNode> GetNodes(HtmlDocument document)
		{
			var parents = GetAllDocumentNodes(document);
			var allNodes = new List<HtmlNode>();
			GetChildNodes(parents, ref allNodes); 
			allNodes.AddRange(parents);

			return allNodes;
		}

		public async Task<List<string>> FindDepartmentsOfAFaculty(string site)
		{
			var page = GetPage(site);
			var report = "Address: " + site + "\r\n";

			string startFilter = "node => ";

			var conditions = _document.GetElementsByTagName("criteria");

			foreach (XmlNode node in conditions)
			{
				var condition = "\"" + node.InnerText + "\"";
				startFilter += "node.InnerText.Contains(" + condition + ")" + " || ";
			}

			var filter = startFilter.Remove(startFilter.Length - 3, 3);
			var options = ScriptOptions.Default.AddReferences(typeof(HtmlNode).Assembly);
			Func<HtmlNode, bool> criteria = await CSharpScript.EvaluateAsync<Func<HtmlNode, bool>>(filter, options);

			var parameters = new CrawlParameters()
			{
				CrawlCriteria = criteria,
				Depth = _depth,
				FindNodes = GetNodes,
				Page = page,
				Link = site
			};

			report = "Details about each link visited for website:" + "\r\n";
			WriteReport(report);

			CrawlPage(parameters: ref parameters);

			report = "Total relevant nodes found: " + parameters.Nodes.Count + "\r\n";

			WriteReport(report);

			var dedup = parameters.Nodes.GroupBy(x => x.InnerText)
				.Select(g => g.OrderBy(o => o.InnerText).First())
				.ToList();


			dedup.RemoveAll(x =>
				x.InnerText.Contains("<") 
				|| x.InnerText.Contains(">") 
				|| x.InnerText.Contains("&")
				|| x.InnerText.Contains("jQuery")
				|| x.InnerText.Contains("#")
				|| x.InnerText.Contains("window.")
				|| x.InnerText.Contains(")")
				|| x.InnerText.Contains("(")
				);

			var nodes = dedup;
			var result = new List<string>();

			var memory = Utility.BytesToMb(GC.GetTotalMemory(false));
			if (memory > MaxMemoryAllocated)
				MaxMemoryAllocated = memory;

			report = "Memory allocated after crawling pages above: " + memory + " MB" + "\r\n\r\n\r\n";
			report += "-----------------------";
			WriteReport(report);

			foreach (var node in nodes)
			{
				var deparment = node.InnerText;

				deparment = (deparment as string).Replace("\n", " ");
				deparment = (deparment as string).Replace("\t", " ");

				var extracted = Utility.ExtractDepartments(deparment).Result;
				var extractedDedup = extracted.GroupBy(x => x)
					.Select(g => g.OrderBy(o => o).First())
					.ToList();

				foreach (var department in extractedDedup)
				{
					result.Add(department);
				}
			}

			var withoutDuplicates = result.GroupBy(x => x)
				.Select(g => g.OrderBy(o => o).First())
				.ToList();

			var title = GetTitle(page);

			if (title == null)
				return withoutDuplicates;

			List<string> titleList = new List<string>();
			titleList.Add(title.InnerText);
			titleList.AddRange(withoutDuplicates);

			return titleList;
		}

		public string FindUniversityDepartments(string root)
		{

			string report = "Starting finding university faculties websites.. \r\n" ;
			WriteReport(report);

			WriteReport("Started from address: " + root + "\r\n");
			var websites = GetFacultiesWebsites(root);
			var res = "";
			report = string.Empty;

			Stopwatch sw = new Stopwatch();
			double memory = GC.GetTotalMemory(false);
			var toMB = Utility.BytesToMb(memory);
			if (toMB > MaxMemoryAllocated)
				MaxMemoryAllocated = toMB;
				
			report = "Allocated memory after finding university websites: " + toMB + " MB\n\n";
			WriteReport(report);

			sw.Start();

			try
			{
				foreach (var site in websites.Result)
				{
					WriteReport("Next faculty website to be searched: " + site);
					var departments = FindDepartmentsOfAFaculty(site).Result;
					res +=
						"____________________________________________________________________________________________________________________________" +
						"\n\n";

					foreach (var department in departments)
					{
						res += department + "\n\n\n";
					}
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
			

			sw.Stop();
			memory = GC.GetTotalMemory(false);
			toMB = Utility.BytesToMb(memory);

			if (toMB > MaxMemoryAllocated)
				MaxMemoryAllocated = toMB;

			report = "Allocated memory after finding departments: " + toMB + " MB\r\n";
			report += "____________________________________________________________________________________________________________________________" + "\n\n";
			WriteReport(report);

			report = "It took: " + sw.Elapsed.TotalSeconds + " seconds to find departments based on potential faculty websites.\r\n";
			report += "Found " + FacultiesFound + " potential faculty websites." + "\r\n";
			report += "Maximum memory allocated: " + MaxMemoryAllocated + " MB.\r\n";
			report += "Total nodes traversed: " + TotalNodesTraversed + ".\r\n";
			report += "Nodes removed: " + this.NodesRemoved + " .\r\n";
			report += "Visited " + this.LinksTraversed + " links.\r\n";
			WriteReport(report);

			return res;
		}

		#endregion

	}
}

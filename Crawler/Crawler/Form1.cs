﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace Crawler
{
	public partial class Form1 : Form
	{
		private Crawler _crawler;
		private string result = string.Empty;

		public Form1()
		{
			InitializeComponent();
			this._crawler = new Crawler();
			string report;
			double memory = GC.GetTotalMemory(false);
			report = "Memory allocated at start of a program: " + Utility.BytesToMb(memory) + " MB" + "\r\n\r\n\r\n";
			_crawler.WriteReport(report);
			report =
				"____________________________________________________________________________________________________________________________";
			_crawler.WriteReport(report);
		}

		private void button3_Click(object sender, EventArgs e)
		{
			result = _crawler.FindUniversityDepartments(_crawler.GetRootWebsite());
			this.richTextBox1.Text += result;
			_crawler.InitReportParams();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();

			saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
			saveFileDialog.FilterIndex = 2;
			saveFileDialog.RestoreDirectory = true;

			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				string path = saveFileDialog.FileName;
				using (System.IO.StreamWriter file =
					new System.IO.StreamWriter(path))
				{
					file.Write(result);
				}

			}
		}
	}
}

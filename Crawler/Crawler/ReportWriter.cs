﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler
{
	public sealed class ReportWriter
	{
		private static volatile ReportWriter instance;
		private static object syncRoot = new Object();
		private static readonly string _reportFilePath = "../../../report.txt";

		private ReportWriter()
		{
		}

		public static ReportWriter Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						System.IO.File.WriteAllText(_reportFilePath, string.Empty);
						if (instance == null)
							instance = new ReportWriter();
					}
				}

				return instance;
			}
		}

		public void WriteReport(string report)
		{
			using (System.IO.StreamWriter file = new System.IO.StreamWriter(File.Open(_reportFilePath, FileMode.Append), Encoding.Unicode))
			{
				file.WriteLine(report);
			}
		}
	}
}

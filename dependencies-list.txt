Probleme koji se mogu javiti zbog nekompatibilnih verzija .NET framework-a je moguće otkloniti dodavanjem sledećih paketa:

Install-Package System.ValueTuple -Version 4.5.0

Install-Package NETStandard.Library.NETFramework -Version 2.0.0-preview1-25305-02